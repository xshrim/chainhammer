# other projects & blockchain benchmarks
https://gitlab.com/electronDLT/chainhammer has helped to create these measurements, on different Ethereum type blockchains. Feel free to include it into your project, preferably as a [submodule](https://www.google.co.uk/search?q=git+submodule+how+to), in case this repo keeps on evolving.

To appear in the following lists, please [fork](https://gitlab.com/electronDLT/chainhammer/forks/new) this repo, add your information, and then "pull request". Thanks.
## projects using chainhammer
* [log.md](log.md) = initial results (April, May 2018)
* "**Quorum stress-test 1: 140 TPS**" by vasa (@vaibhavsaini, @vasa-develop, @towardsblockchain) (May 2018)
  * [medium article](https://medium.com/@vaibhavsaini_67863/792f39d0b43f) = instruction, screenshots, results
  * [github repo](https://github.com/vasa-develop/quorum-testnode-1) = compilation from different repos: an updated [7nodes example](https://github.com/jpmorganchase/quorum-examples/pull/93), this chainhammer repo, and an [installation manual](https://gist.github.com/vasa-develop/ff34688c7cb7ae8bb6de9587a4752969#file-dependencies-sh) for Ubuntu AWS
* ...

please add yours at the bottom, and pull request

## other blockchain benchmarking results & source codes

* ...

please add yours, and pull request
